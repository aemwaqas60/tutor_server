const uploadFile = require('express-fileupload');

module.exports = (req, res, next) => {

    console.log("================= FILE HANDLER ==================");



    if (!req.files) {
        req.fileName = null;
        next();
    } else {

        let fileName = Date.now().toString() + req.files.image.name;
        let file = req.files.image;
        const server_address = '/home/ubuntu/tutor_server/uploads/';
        const local_address = './uploads/';


        file.mv(server_address + fileName, (err) => {
            if (err) {
                fileName = null;
                return res.status(500).json({
                    status: "500",
                    message: "file upload error",
                    error: err
                });
            }
            req.fileName = fileName;
            next();
        });

    }

}