const FCM = require('fcm-push');




//console.log("========--------FireBase Notification Helper------==========");

module.exports = {

    send(t, m, tokens) {

        console.log("Message title:", t);
        console.log("Message:", m);
        console.log("Device tokens:", tokens);


        // fcm api key
        var serverKey = 'AAAADl0f6_8:APA91bHcVC1WUH82mRDg5bQYNDBkQsPU7Ohs8piDMmam3rrzbZV4kJeBAPocvPJwXR6BkJ650Dp6e4lsFZSNwwwY-7nO9WIen31dK22GD8Cn-_ZpeeoFh60lH004VhWe5gN8_M55tmU-';
        var fcm = new FCM(serverKey);

        tokens.forEach(token => {


            var message = {
                to: token, // required fill with device token or topics
                collapse_key: 'your_collapse_key',
                data: {
                    your_custom_data_key: 'your_custom_data_value'
                },
                notification: {
                    title: t,
                    body: m
                }
            };

            //callback style
            /*        fcm.send(message, function (err, response) {
                       if (err) {
                           console.log("Something has gone wrong!");
                       } else {
                           console.log("Successfully sent with response: ", response);
                       }
                   }); */

            //promise style
            fcm.send(message)
                .then(function (response) {
                    console.log("Successfully sent with response: ", response);
                    return response;
                })
                .catch(function (err) {
                    console.log("Something has gone wrong!");
                    console.error(err);
                    return err;

                });

        });


    }

}