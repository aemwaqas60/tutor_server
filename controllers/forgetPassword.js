const User = require('../models/user');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const app = require("../app");
var nodemailer = require('nodemailer');

module.exports = {


    checkEmail(req, res) {


        let email = req.body.email;
        // nodemailer configrations 
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'afrahtayyab27@gmail.com',
                pass: 'aiekltilbufyvhgp'
            }
        });

        var code = Math.floor(1000 + Math.random() * 9000);
        console.log(code);

        // nodemailer configurations
        var mailOptions = {
            from: 'afrahtayyab27@gmail.com',
            to: email,
            subject: 'Reset YOUR TUTOR APP password',
            text: `please use this "${code}" 4 digit verification code to reset your password`
        };


        User.findOne({
            email: email
        }).then((user) => {

            console.log(user)

            if (!user) {
                res.status(404).json({
                    status: "401",
                    message: "user not found,please enter valid email id",
                    data: user
                })
            } else {

                // Send email.
                transporter.sendMail(mailOptions, function (err, info) {
                    if (err) {
                        res.status(404).json({
                            status: "404",
                            message: "email not sent,use valid email id",
                            error: err
                        })
                    } else {
                        res.status(200).json({
                            status: "200",
                            message: "email sent successfully",
                            user: user,
                            code: code

                        })
                    }
                });
            }


        }).catch((err) => {

            res.status(500).json({
                status: "500",
                message: "something went wrong with getting user | mongodb internal server error",
                error: err
            })
        })
    },


    resetPassword(req, res) {

        const email = req.body.email;
        const newPassword = req.body.password;

        User.findOne({
                email: email
            })
            .then((user) => {

                if (!user) {

                    res.status(404).json({
                        status: "404",
                        message: "User not found,please enter valid email id.",
                        data: user
                    })
                } else {

                    bcrypt.hash(newPassword, 10, (hashError, hash) => {
                        if (hashError) {
                            return res.json({
                                status: "500",
                                status_message: "hash password generation error",
                                error: hashError
                            });
                        } else {

                            user.password = hash;
                            user.save().then((result) => {
                                res.json({
                                    status: "200",
                                    status_message: "password updated successfully",
                                    data: result
                                });
                            }).catch((err) => {
                                res.json({
                                    status: "500",
                                    status_message: "something went saving new password | mongod server error",
                                    error: err
                                });

                            });
                        }

                    })

                }


            })
            .catch((err) => {

                res.status(500).json({
                    status: "500",
                    message: "something went wrong with getting user | mongodb internal server error",
                    error: err
                })

            })

    }
}