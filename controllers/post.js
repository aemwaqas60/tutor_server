const uploadFile = require('express-fileupload');
const Post = require('../models/post');
const User = require('../models/user');
const Comment = require('../models/comment');
const path = require('path');
const uploads = path.join(__dirname, '/uploads/');



console.log("===========Post Controller===========");

module.exports = {

    addNewPost(req, res) {

        const post_text = req.body.text;
        let _id = req.body.user_id;

        let fileName = req.fileName;

        User.findById({
                _id: _id
            }).then((user) => {

                if (!user) {
                    return res.status(401).json({
                        status: "401",
                        message: "User not found,please send valid user id",
                    });
                }
                const post = {
                    text: post_text,
                    image: fileName,
                    _user: user
                }
                Post.create(post)
                    .then((post) => {
                        console.log(post);
                        res.status(200).json({
                            status: "200",
                            message: "post successfully added",
                            post: post
                        })
                    }).catch((err) => {
                        return res.status(500).json({
                            status: "500",
                            message: "something went wrong saving new post | mongodb server error",
                            error: err
                        })
                    })
            })
            .catch((err) => {
                res.status(500).json({
                    status: "500",
                    message: "something went wrong with finding user | mongodb internal server error",
                    error: err
                });
            })
    },

    getPosts(req, res, next) {
        const page = req.params.page;
        //const user_id = req.params.user_id;

        Post.find({})
            .populate('_user', "username image email type")
            .populate('comments._user', "username image email type")
            .limit(50)
            .sort({
                'created_date': -1
            })
            .skip((page - 1) * 50)
            .then((posts) => {
                res.status(200).json({
                    status: "200",
                    message: "post sent successfully",
                    data: posts
                })

            }).catch((err) => {
                res.status(500).json({
                    status: "500",
                    message: "something went wrong | mongodb internal server error",
                    error: err
                })
            })

    },
    addCommentOnPost(req, res, next) {

        console.log(req.body);
        console.log(req.body.user_id);
        console.log(req.body.post_id);
        console.log(req.body.text);

        let post_id = req.params.post_id;
        let user_id = req.body.user_id;
        let text = req.body.text;


        User.findById({
                _id: user_id
            }).then((user) => {

                Post.findByIdAndUpdate({
                        _id: post_id
                    }, {
                        $push: {
                            comments: {
                                text: text,
                                _user: user
                            }
                        }
                    })
                    .then((post) => {

                        res.status(200).json({
                            status: "200",
                            message: "comment added on post successfully",
                        })
                    }).catch((err) => {

                        res.status(500).json({
                            status: "500",
                            message: "something went wrong | mongodb server error",
                            error: err
                        })

                    })

            })
            .catch((err) => {
                res.status(500).json({
                    status: "500",
                    message: "something went wrong | mongodb server error",
                    error: err
                })
            })

    },

    likePost(req, res, next) {

        let post_id = req.params.post_id;
        let user_id = req.body.user_id;


        Post.findByIdAndUpdate({
            _id: post_id
        }, {
            $push: {
                like_by: {
                    user_id: user_id
                },

            },
            $inc: {
                likes: 1
            }
        }).then((post) => {
            res.status(200).json({
                status: "200",
                message: "post successfully liked",
                data: post
            });

        }).catch((err) => {
            res.status(500).json({
                status: "500",
                message: "something went wrong on server | mongdb server error",
                error: err
            });
        })

    },

    unlikePost(req, res, next) {

        let post_id = req.params.post_id;
        let user_id = req.body.user_id;


        Post.findByIdAndUpdate({
            _id: post_id
        }, {
            $pull: {
                like_by: {
                    user_id: user_id
                },

            },
            $inc: {
                likes: -1
            }
        }).then((post) => {
            res.status(200).json({
                status: "200",
                message: "post successfully unliked",
                data: post
            });

        }).catch((err) => {
            res.status(500).json({
                status: "500",
                message: "something went wrong on server | mongdb server error",
                error: err
            });
        })

    }
}