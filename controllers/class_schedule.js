const Class_Schedule = require('../models/class_schedule');


module.exports = {

    getSchedules(req, res, next) {
        const id = req.params.id;

        Class_Schedule.find({
                $or: [{
                    _student: id
                }, {
                    _tutor: id
                }]
            }).sort({
                'created_date': -1
            })
            .populate('_student', 'username email image type')
            .populate('_tutor', 'username email image type')
            .then((schedules) => {

                res.status(200).json({
                    status: "200",
                    message: " schedules found successflly",
                    data: schedules
                })

            }).catch((err) => {
                res.status(500).json({
                    status: "500",
                    message: "something went wrong with getting schedules | mongdb internal server error",
                    error: err
                })

            })

    },

}