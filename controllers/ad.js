const Ad = require('../models/ad')
const User = require('../models/user')

module.exports = {

    addNewAd(req, res, next) {

        let user_id = req.params.user_id;

        User.findOne({
            _id: user_id
        }).then((user) => {
            let newAdd = req.body;
            newAdd._user = user;

            Ad.create(newAdd)
                .then((ad) => {

                    res.status(200).json({
                        status: "200",
                        message: "Ad saved successfully",
                        data: ad
                    });

                }).catch((err) => {
                    res.status(500).json({
                        status: "500",
                        message: "something went wrong saving new ad| mongodb server error",
                        error: err
                    });

                })

        }).catch((err) => {
            res.status(500).json({
                status: "500",
                message: "something went wrong to find user| mongodb server error",
                error: err
            });

        })

    },


    getAds(req, res, next) {

        let page = req.params.page;

        Ad.find({})
            .limit(50)
            .skip((page - 1) * 50)
            .sort({
                'created_date': -1
            })
            .populate('_user ')
            .then((ads) => {
                res.status(200).json({
                    status: "200",
                    message: "ads getting successfully",
                    data: ads
                });
            }).catch((err) => {
                res.status(500).json({
                    status: "500",
                    message: "something went wrong getting ads| internal mongodb server error",
                    error: err
                });
            })

    }

}