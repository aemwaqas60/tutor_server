const uploadFile = require('express-fileupload');
const Post = require('../models/post');
const User = require('../models/user');
const Comment = require('../models/comment');
const Class_Schedule = require('../models/class_schedule');

console.log("===========Tutor Controller===========");

module.exports = {
    getTutors(req, res, next) {
        const page = req.params.page;

        User.find({
                type: 'tutor'
            })
            .limit(50)
            .skip((page - 1) * 50)
            .then((users) => {
                res.status(200).json({
                    status: "200",
                    message: "tutor sent successfully",
                    data: users
                })

            }).catch((err) => {
                res.status(500).json({
                    status: "500",
                    message: "something went wrong | mongodb internal server error",
                    error: err
                });
            })

    },

    addRating(req, res, next) {

        let ratting = req.body.ratting;
        let tutor_id = req.params.tutor_id;

        User.findByIdAndUpdate({_id:tutor_id},{
            $set:{
                rating:ratting
            }
        }).then((user)=>{

            if(user.length<1){
                res.status(401).json({
                    status: "401",
                    message: "user not found,please use valid user _id"
                })
            }

            else{
                res.status(200).json({
                    status: "200",
                    message: "Rating successfully added."
                })

            }



        }).catch((err) => {

            res.status(500).json({
                status: "500",
                message: "something went wrong | mongodb internal server error",
                error: err
            });


        })



    },
    getTutorDetails(req, res, next) {
        const tutor_id = req.params.tutor_id;


        User.findOne({
            _id: tutor_id
        }).then((tutor) => {
            console.log(tutor);

            if (!tutor) {
                return res.status(401).json({
                    status: "401",
                    message: "Tutor not found | please send valid tutor _id"
                });

            } else {

                Class_Schedule.find({
                    _tutor: tutor._id
                }).then((data) => {
                    res.status(200).json({
                        status: "200",
                        message: "tutor found successfully.",
                        tutor: tutor,
                        Class_Schedules: data
                    });

                }).catch((err) => {
                    res.status(500).json({
                        status: "500",
                        message: "something went wrong with getting tutor schedules | mongodb internal server error",
                        error: err
                    });
                })
            }
        }).catch((err) => {

            res.status(500).json({
                status: "500",
                message: "something went wrong | mongodb internal server error",
                error: err
            });

        })


    }

}