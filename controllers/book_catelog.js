const Books = require("../models/books");

module.exports = {
    uploadBook(req, res, next) {


        console.log(req.body);
        console.log(req.fileName);
        req.body.file_path = req.fileName;


        Books.create(req.body).then((data) => {

            res.json({
                status: "200",
                status_message: "book uploaded successfully"
            });

        }).catch((err) => {

            res.status(500).json({
                status: "500",
                status_message: "something went worong to upload book | mongodb interval server error",
                error: err
            });
        });

    },


    getBooks(req, res, next) {


        Books.find({
            subject: req.params.subject
        }).then((books) => {

            if (books.length < 1) {
                res.json({
                    status: "200",
                    status_message: "No book found with this subject",
                    data: books
                });
            } else {

                res.json({
                    status: "200",
                    status_message: "books found successfully",
                    data: books
                });
            }

        }).catch((err) => {

            res.status(500).json({
                status: "500",
                status_message: "something went worong to find books | mongodb interval server error",
                error: err
            });

        })

    }
}