const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("../models/user");
const Auth = require('../models/auth');

exports.user_signup = (req, res, next) => {
  console.log("============ Signup  Controller =========");
  var email = req.body.email;
  User.find({
      email: email
    })
    .then(user => {
      if (user.length >= 1) {
        return res.json({
          status: "409",
          status_message: "User already Exists,try with other email."
        });
      } else {
        bcrypt.hash(req.body.password, 10, (hashError, hash) => {
          if (hashError) {
            return res.status(403).json({
              status: "403",
              status_message: "hash password generation error",
              error: hashError
            });
          } else {

            const newUser = req.body;
            newUser.password = hash;

            User
              .create(newUser)
              .then(new_user => {

                console.log(new_user)

                jwt.sign({
                    email: newUser.email
                  },
                  process.env.JWT_TOKEN, (jwtError, token) => {
                    console.log("Inside JWT_TOKEN");

                    if (jwtError) {
                      return res.status(403).json({
                        status: "403",
                        status_message: "Jwt token generation error",
                        error: jwtError
                      });
                    } else {

                      const player_id = req.body.player_id;
                      const newAuth = {
                        _user: new_user,
                        user_email: new_user.email,
                        player_id: player_id,
                        token: token
                      }

                      Auth.create(newAuth).then((auth) => {

                        res.status(200).json({
                          status: "200",
                          status_message: "new user Signup successfully",
                          user: new_user,
                          auth: auth
                        });

                      }).catch((err) => {
                        res.json({
                          status: "500",
                          status_message: "something went wrong at saving new auth with new user | mongod server error",
                          error: err
                        });
                      });
                    }
                  }
                );
              })
              .catch((err) => {
                res.json({
                  status: "500",
                  status_message: "something went wrong at saving new user | mongod server error",
                  error: err
                });
              });
          }
        });
      }
    })
    .catch((err) => {
      res.json({
        status: "500",
        status_message: "something went worong | mongo server error",
        error: err
      });
    });
};

exports.user_sigin = (req, res, next) => {
  console.log("============ Signin Controller ==========");
  let email = req.body.email;

  User.find({
      email: email
    })
    .then(user => {
      if (user.length < 1) {
        return res.json({
          status: "401",
          status_message: "Auth Failed,Invalid username or password"
        });
      } else {

        bcrypt.compare(req.body.password, user[0].password, (err, result) => {

          if (result) {

            jwt.sign({
                email: user[0].email
              },
              process.env.JWT_TOKEN, (jwtError, token) => {

                if (jwtError) {
                  return res.status(403).json({
                    status: "403",
                    status_message: "Jwt token generation error",
                    error: jwtError
                  });

                } else {
                  // Checking user device id in auths table 
                  Auth.findOne({
                    player_id: req.body.player_id,
                    user_email: email
                  }).then((auth) => {

                    // if user signin with new device then create new aut with new device id in auts table
                    //if user change his email id then also create new auth with new email id in auth table
                    if (!auth || auth.length < 1) {
                      console.log("inside if");


                      const player_id = req.body.player_id;
                      const newAuth = {
                        _user: user[0],
                        user_email: email,
                        player_id: player_id,
                        token: token
                      }

                      Auth.create(newAuth).then((auth) => {

                        res.status(200).json({
                          status: "200",
                          status_message: "user Signin successfully with new device",
                          user: user[0],
                          auth: auth
                        });

                      }).catch((err) => {
                        res.json({
                          status: "500",
                          status_message: "something went wrong at saving new auth with new device id | mongod server error",
                          error: err
                        });
                      });

                    } else {

                      Auth.findByIdAndUpdate({
                        _id: auth._id
                      }, {
                        $set: {
                          isLogin: true,
                          token: token,
                          last_login: new Date()
                        }
                      }).then((updated_auth) => {

                        res.status(200).json({
                          status: "200",
                          status_message: "user singin successfully with exising device",
                          user: user[0],
                          auth: auth
                        });
                      }).catch((err) => {
                        res.status(500).json({
                          status: "500",
                          status_message: "something went wrong at saving new auth with exisiting device | mongod server error",
                          error: err
                        });

                      });
                    }

                  }).catch((err) => {
                    res.json({
                      status: "500",
                      status_message: "something went wrong at getting user auts info | mongod server error",
                      error: err
                    });
                  });
                }
              }
            );

          } else {
            res.json({
              status: "401",
              status_message: "Auth Failed,Invalid username or password"
            });
          }
        });
      }
    })
    .catch((err) => {
      res.json({
        status: "500",
        status_message: "something went wrong | mongodb server error",
        error: err
      });
    });
};



exports.user_logout = (req, res, next) => {

  const email = req.body.email;
  const player_id = req.body.player_id;

  if (!email || !player_id) {

    return res.json({
      status: "403",
      status_message: "missing fields error",
    });
  }

  Auth.findOne({
    user_email: email,
    player_id: player_id
  }).then((auth) => {

    console.log(auth);

    if (!auth) {

      return res.status(403).json({
        status: "403",
        status_message: "auth not found,please enter valid email and player_id",
      });

    } else {

      Auth.findByIdAndUpdate({
        _id: auth._id
      }, {
        $set: {
          isLogin: false,
          token: null
        }
      }).then((auth1) => {

        res.status(200).json({
          status: "200",
          status_message: "user logged out successfully",
          data: auth1
        });

      }).catch((err) => {

        res.json({
          status: "500",
          status_message: "something went wrong updating user auth | mongodb server error",
          error: err
        });

      })
    }

  }).catch((err) => {

    res.json({
      status: "500",
      status_message: "something went wrong getting user auth | mongodb server error",
      error: err
    });

  })

}