const User = require('../models/user')
const Auth = require('../models/auth')
const Tution = require('../models/tution')
const Class_Schedule = require('../models/class_schedule')
const fcm = require('../helpers/FCM_helper');
module.exports = {

    newTutionRequest(req, res, next) {

        let student_id = req.body._student;
        let tutor_id = req.body._tutor;

        User.findById({
                _id: student_id
            }).then((student) => {


                if (!student) {
                    return res.status(404).json({
                        status: "404",
                        message: "Sudent not found,please send valid student id",
                    });
                }
                User.findById({
                        _id: tutor_id
                    })
                    .then((tutor) => {

                        if (!tutor) {
                            return res.status(404).json({
                                status: "404",
                                message: "Tutor not found,Pleas send valid tutor id",
                            });
                        }

                        req.body._student = student;
                        req.body._tutor = tutor;

                        Tution.create(req.body)
                            .then((tution) => {


                                //console.log("tutor.email: ", tutor.email);

                                Auth.find({
                                    user_email: tutor.email

                                }).then((auths) => {

                                    //  console.log("Inside auth ");


                                    let player_ids = [];
                                    let title =  'Student send a new request';
                                    let message = req.body.description;


                                    // console.log("message inside auth", message);

                                    auths.forEach((auth) => {
                                        if (auth.isLogin === true) {
                                            player_ids.push(auth.player_id);
                                        }
                                    });

                                   let response=fcm.send(title, message, player_ids);
                                    //console.log(response);
                                    res.status(200).json({
                                        status: "200",
                                        status_message: "Tution request saved successfully",
                                       // data:tution

                                    });

                                }).catch((err) => {
                                    res.status(500).json({
                                        status: "500",
                                        message: "something went wrong find auth | mongodb server error",
                                        error: err
                                    });
                                })

                            })
                            .catch((err) => {

                                res.status(500).json({
                                    status: "500",
                                    message: "something went wrong to save new Tution request | internal mongodb server error",
                                    error: err
                                });

                            })

                    })
                    .catch((err) => {

                        res.status(500).json({
                            status: "500",
                            message: "something went wrong getting turor |internal mongodb server error",
                            error: err
                        });

                    })
            })
            .catch((err) => {
                res.status(500).json({
                    status: "500",
                    message: "something went wrong getting student | internal mongodb server error",
                    error: err
                });
            })



    },

    getTutionRequests(req, res, next) {
        let _id = req.params.user_id;

        Tution.find({
                $or: [{
                    _student: _id
                }, {
                    _tutor: _id
                }]
            })
            .sort({
                'created_date': -1
            })
            .populate('_student', 'username email image type')
            .populate('_tutor', 'username email image type').then((tutions) => {
                res.status(200).json({
                    status: "200",
                    message: "Tution requests found successfully",
                    data: tutions
                });

            })
            .catch((err) => {

                res.status(500).json({
                    status: "500",
                    message: "something went wrong getting user | internal mongodb server error",
                    error: err
                });

            })

    },

    updateTutionRequest(req, res, next) {

        let tution_id = req.params.tution_id;
        let status = req.body.status;

        Tution.findByIdAndUpdate({
                _id: tution_id
            }, {
                $set: {
                    status: status
                }
            }).populate('_student')
            .populate('_tutor')
            .then((tution) => {

                if (!tution) {
                    return res.status(401).json({
                        status: "401",
                        message: "Tution Request not found,please send valid tution id",
                        data: tution
                    });
                } else {

                    Auth.find({
                        user_email: tution._student.email,
                        isLogin: true
                    }).then((auths) => {

                        let player_ids = [];
                        let title;
                        let message;
                        console.log('auths', auths);
                        auths.forEach((auth) => {
                            player_ids.push(auth.player_id);
                        });


                        if (status === "accepted") {
                            title = "Tution  Request Accepted";
                            message = "you tution request" + tution.description + "has accepted by" + tution._tutor.username;
                            let response = fcm.send(title, message, player_ids);

                        } else if (status === "rejected") {

                            title = "Tution Request Reject";
                            message = "you request" + tution.description + "has Rejected by" + tution._tutor.username;
                            let response = fcm.send(title, message, player_ids);

                        }

                    }).catch((err) => {
                        return res.status(500).json({
                            status: "500",
                            message: "something went wrong find student auth to send notifications | mongodb server error",
                            data: tution
                        });
                    })


                    if (status === "accepted") {
                        let newSchedule = {
                            time: tution.time,
                            days: tution.days,
                            _tutor: tution._tutor,
                            _student: tution._student,
                            subject: tution.subject,
                        }
                        Class_Schedule.create(newSchedule).then((schedule) => {

                            res.status(200).json({
                                status: "200",
                                message: "Tution Request upadated successfully,also created new schedule with student",
                                tution: tution,
                                schedule: schedule,
                                student: tution._user
                            });

                        }).catch((err) => {
                            res.status(500).json({
                                status: "500",
                                message: "something went wrong with creating new schedule | mongodb internal server error ",
                                error: err
                            });
                        })

                    } else {
                        res.status(200).json({
                            status: "200",
                            message: "Tution Request upadated successfully",
                            data: tution
                        });
                    }
                }
            })
            .catch((err) => {

                res.status(500).json({
                    status: "500",
                    message: "something went wrong getting request | internal mongodb server error",
                    error: err
                });

            })
    },

}