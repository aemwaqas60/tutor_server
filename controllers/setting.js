const uploadFile = require('express-fileupload');
const bcrypt = require("bcrypt");
const Post = require('../models/post');
const User = require('../models/user');
const Comment = require('../models/comment');
const Auth = require('../models/auth');




console.log("===========Settings Controller===========");

module.exports = {

    updateSettings(req, res, next) {


        let _id = req.params.user_id;

        if (req.fileName !== null) {

            console.log(req.fileName);
            req.body.image = req.fileName;
        }

        if (req.body.password) {
            console.log(req.body);

            bcrypt.hash(req.body.password, 10, (hashError, hash) => {
                if (hashError) {
                    return res.status(403).json({
                        status: "403",
                        status_message: "hash password generation error",
                        error: hashError
                    });
                } else {

                    req.body.password = hash;

                    User.findByIdAndUpdate({
                            _id: _id
                        }, {
                            $set: req.body
                        })
                        .then((user) => {

                            if (user.length < 1) {

                                res.status(500).json({
                                    status: "401",
                                    message: "user not found,please use valid user _id",
                                })

                            } else {
                                User.findOne({
                                    _id: user._id
                                }).then((updated_user) => {

                                    if (req.body.email) {
                                        Auth.updateMany({
                                            user_email: user.email
                                        }, {
                                            $set: {
                                                user_email: req.body.email
                                            }

                                        }).then((updated_auth) => {
                                            res.status(200).json({
                                                status: "200",
                                                message: "user and auth settings updated successfully",
                                                user: updated_user,
                                                updated_auth: updated_auth
                                            })

                                        }).catch((err) => {
                                            res.status(500).json({
                                                status: "500",
                                                message: "something went wrong with updating user email in auths | mongodb server error",
                                                user: updated_user
                                            });
                                        });

                                    } else {
                                        res.status(200).json({
                                            status: "200",
                                            message: "settings updated successfully",
                                            user: updated_user
                                        })
                                    }
                                }).catch((err) => {
                                    res.status(500).json({
                                        status: "500",
                                        message: "something went wrong finding updated user| mongodb internal server error",
                                        error: err
                                    });

                                })
                            }

                        }).catch((err) => {
                            res.status(500).json({
                                status: "500",
                                message: "something went wrong finding user| mongodb internal server error",
                                error: err
                            });
                        })
                }
            })

        } else {

            console.log(req.body);


            User.findByIdAndUpdate({
                    _id: _id
                }, {
                    $set: req.body
                })
                .then((user) => {

                    if (user.length < 1) {

                        res.status(401).json({
                            status: "401",
                            message: "user not found,please use valid user _id"
                        })

                    } else {
                        User.findOne({
                            _id: user._id
                        }).then((updated_user) => {

                            if (req.body.email) {
                                Auth.updateMany({
                                    user_email: user.email
                                }, {
                                    user_email: req.body.email
                                }).then((updated_auth) => {
                                    res.status(200).json({
                                        status: "200",
                                        message: "user and auth settings updated successfully",
                                        user: updated_user,
                                        updated_auth: updated_auth
                                    })

                                }).catch((err) => {
                                    res.status(500).json({
                                        status: "500",
                                        message: "something went wrong with updating user email in auths | mongodb server error",
                                        user: updated_user
                                    });

                                });

                            } else {
                                res.status(200).json({
                                    status: "200",
                                    message: "settings updated successfully",
                                    user: updated_user
                                })

                            }

                        }).catch((err) => {
                            res.status(500).json({
                                status: "500",
                                message: "something went wrong finding updated user| mongodb internal server error",
                                error: err
                            });

                        })
                    }
                }).catch((err) => {
                    res.status(500).json({
                        status: "500",
                        message: "something went wrong finding user| mongodb internal server error",
                        error: err
                    })
                })

        }


    },


    getUserSettings(req, res, next) {

        let id = req.params.user_id;

        User.findOne({
            _id: id
        }).then((user) => {

            if (!user) {

                return res.status(403).json({
                    status: "403",
                    status_message: "user not found,please send valid user id",
                })

            } else {

                res.status(200).json({
                    status: "200",
                    status_message: "user found successfully",
                    data: user
                })

            }



        }).catch((err) => {
            res.status(500).json({
                status: "500",
                status_message: "something went wrong with getting user | mongodb internal server error",
                error: err
            })

        })

    }
}