const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = require('../models/user');

const PostSchema = new Schema({

    text: {
        type: String,
        default: null
    },
    image: {
        type: String,
        default: null
    },
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    likes: {
        type: Number,
        default: 0
    },
    isLiked: {
        type: Number,
        default: 0
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    like_by: [{
        user_id: {
            type: String,
            default: null
        }

    }],
    comments: [{
        text: {
            type: String,
            default: null
        },
        created_date: {
            type: Date,
            default: Date.now
        },
        _user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    }]

});

const Post = mongoose.model('Post', PostSchema);
module.exports = Post;