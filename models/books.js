const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BooksSchema = new Schema({

    book_name: {
        type: String,
        default: null
    },
    subject: {
        type: String,
        default: null
    },
    author: {
        type: String,
        default: null,
    },
    file_path: {
        type: String,
        default: null,
    },
    created_date: {
        type: Date,
        default: Date.now
    }

});

const Books = mongoose.model('Books', BooksSchema);
module.exports = Books;