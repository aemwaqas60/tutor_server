const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdSchema = new Schema({

    title: {
        type: String,
        default: null,
        required: true
    },
    subject: {
        type: String,
        default: null
    },
    Location: {
        type: String,
        default: null
    },
    number: {
        type: String,
        default: null
    },

    description: {
        type: String,
        default: null
    },

    created_date: {
        type: Date,
        default: Date.now
    },

    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }

});

const Ad = mongoose.model('Ad', AdSchema);
module.exports = Ad;