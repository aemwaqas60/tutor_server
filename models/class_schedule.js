const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClassScheduleSchema = new Schema({

    time: {
        type: String,
        default: null
    },
    days: {
        type: String,
        default: null
    },
    subject: {
        type: String,
        default: null,
    },
    _tutor: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    _student: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }

});

const ClassSchedule = mongoose.model('ClassSchedule', ClassScheduleSchema);
module.exports = ClassSchedule;