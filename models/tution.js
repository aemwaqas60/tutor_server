const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TutionSchema = new Schema({
    
    subject: {
        type: String,
        default: null,
        required: true
    },
    description: {
        type: String,
        default: null
    },
    time: {
        type: String,
        default: null,
        required: true
    },
    days: {
        type: String,
        default: null,
        required: true
    },
    status: {
        type: String,
        default: 'pending'
    },
    _student: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    _tutor: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    created_date: {
        type: Date,
        default: Date.now
    }

});

const Tution = mongoose.model('Tution', TutionSchema);
module.exports = Tution;