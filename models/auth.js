const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('../models/user.js');


const AuthSchema = new Schema({
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    user_email: {
        type: String,
        default: null
    },
    player_id: {
        type: String,
        default: null
    },
    token: {
        type: String,
        default: null
    },
    last_login: {
        type: Date,
        default: Date.now
    },
    isLogin: {
        type: Boolean,
        default: true
    }
});

const Auth = mongoose.model('Auth', AuthSchema);
module.exports = Auth;