const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    default: null,
  },
  password: {
    type: String,
    default: null,
    required:true
  },
  email: {
    type: String,
    default: null,
    unique: true
  },
  gender: {
    type: String,
    default: null
  },
  image: {
    type: String,
    default: null
  },
  mobile_no: {
    type: String,
    default: null
  },
  cnic: {
    type: String,
    default: null
  },
  subject: {
    type: String,
    default: null
  },
  institution_name: {
    type: String,
    default: null
  },
  education: {
    type: String,
    default: null
  },
  area: {
    type: String,
    default: null
  },
  experience: {
    type: String,
    default: null
  },
  type: {
    type: String,
    default: null
  },
  available_times: {
    type: String,
    default: null
  },
  available_days: {
    type: String,
    default: null
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  rating:{
    type: Number,
    default: 0
  }
});

const User = mongoose.model("User", UserSchema);

module.exports = User;