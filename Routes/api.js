const express = require('express');
const router = express.Router();
const User_Controller = require('../controllers/user');
const post_Controller = require('../controllers/post');
const tutor_Controller = require('../controllers/tutor');
const setting_Controller = require('../controllers/setting');
const ad_Controller = require('../controllers/ad');
const tution_Controller = require('../controllers/tution');
const forgetPassword_Controller = require('../controllers/forgetPassword');
const class_schedule_Controller = require('../controllers/class_schedule');
const file_handler = require('../helpers/file_handler');
const book_catelog = require('../controllers/book_catelog');

//Auth Routes
router.post('/signup', User_Controller.user_signup);
router.post('/signin', User_Controller.user_sigin);
router.post('/logout', User_Controller.user_logout);

//Post routes
router.post('/post',file_handler, post_Controller.addNewPost);  //add new post 
router.get('/posts/:page', post_Controller.getPosts);
router.post('/commentOnPost/:post_id', post_Controller.addCommentOnPost);
router.post('/likePost/:post_id', post_Controller.likePost);
router.post('/unlikePost/:post_id', post_Controller.unlikePost);

//Tutor Profile routes
router.get('/tutors/:page', tutor_Controller.getTutors);
router.get('/tutor/:tutor_id', tutor_Controller.getTutorDetails);

//Settings routes
router.post('/settings/:user_id', file_handler, setting_Controller.updateSettings);
router.get('/settings/:user_id', setting_Controller.getUserSettings);


// Ad Portal Routes
router.post('/ad/:user_id', ad_Controller.addNewAd);
router.get('/ads/:page', ad_Controller.getAds);

// Tution request routes
router.post('/tution', tution_Controller.newTutionRequest);
router.get('/tutions/:user_id', tution_Controller.getTutionRequests);
router.put('/tution/:tution_id', tution_Controller.updateTutionRequest);
//router.delete('/tution/:id', tution_Controller.deleteTutionRequest);


//forget password routes
router.post('/checkEmailId', forgetPassword_Controller.checkEmail); // to varify user eamil and send 4 digit code on email
router.post('/resetPassword', forgetPassword_Controller.resetPassword); //to reset user password

//class Schedule routes
router.get('/classSchdule/:id', class_schedule_Controller.getSchedules); // to varify user eamil and send 4 digit code on email

// Book Catelog routes
router.post('/uploadBook',file_handler, book_catelog.uploadBook); // to upload new book
router.get('/getBooks/:subject', book_catelog.getBooks); // to get the books by subject

//add rating of tutor
router.post('/addTutorRating/:tutor_id', tutor_Controller.addRating); // to get the books by subject

module.exports = router;